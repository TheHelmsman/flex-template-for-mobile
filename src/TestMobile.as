package{
	import com.view.ViewTestMobile;
	
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageOrientation;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.StageOrientationEvent;
	
	import starling.core.Starling;
	[SWF(width="960",height="640",frameRate="60",backgroundColor="#4a4137")]
	
	public class TestMobile extends Sprite{	
		//--------------------------------------------------------------------------------------------------------- 
		// 
		//  PUBLIC & INTERNAL VARIABLES 
		// 
		//---------------------------------------------------------------------------------------------------------
		
		
		//--------------------------------------------------------------------------------------------------------- 
		//
		// PRIVATE & PROTECTED VARIABLES
		//
		//---------------------------------------------------------------------------------------------------------
		private var _starling:Starling;
		
		//--------------------------------------------------------------------------------------------------------- 
		//
		//  CONSTRUCTOR 
		// 
		//---------------------------------------------------------------------------------------------------------
		public function TestMobile(){
			super();
			
			if(this.stage)
			{
				this.stage.scaleMode = StageScaleMode.NO_SCALE;
				this.stage.align = StageAlign.TOP_LEFT;
			}
			this.mouseEnabled = this.mouseChildren = false;
			
			this.loaderInfo.addEventListener(Event.COMPLETE, _handlerLoaderInfoComplete);
			
		}
		//--------------------------------------------------------------------------------------------------------- 
		// 
		//  GETTERS & SETTERS  
		// 
		//---------------------------------------------------------------------------------------------------------
		
		
		//--------------------------------------------------------------------------------------------------------- 
		// 
		//  PUBLIC & INTERNAL METHODS 
		// 
		//---------------------------------------------------------------------------------------------------------
		
		
		//--------------------------------------------------------------------------------------------------------- 
		//
		// PRIVATE & PROTECTED METHODS 
		//
		//--------------------------------------------------------------------------------------------------------
		
		private function _initialize():void{
			Starling.handleLostContext = true;
			Starling.multitouchEnabled = true;
			
			this._starling = new Starling(ViewTestMobile, this.stage);
			this._starling.enableErrorChecking = false;
			this._starling.showStatsAt("left", "bottom");
			
			var startOrientation:String = stage.orientation;
			if (startOrientation == StageOrientation.DEFAULT || startOrientation == StageOrientation.UPSIDE_DOWN){
				stage.setOrientation(StageOrientation.ROTATED_RIGHT);
			}else{
				stage.setOrientation(startOrientation);
			}
			
			this._starling.start();
			
			stage.addEventListener(StageOrientationEvent.ORIENTATION_CHANGING, _handlerOrientationChange, false, 0, true);
		}
		
		//--------------------------------------------------------------------------------------------------------- 
		// 
		//  EVENT HANDLERS  
		// 
		//---------------------------------------------------------------------------------------------------------
		/**
		 * handler - stage orientation was changed
		 * */
		private function _handlerOrientationChange(event:StageOrientationEvent) : void {
			if (event.afterOrientation == StageOrientation.DEFAULT || event.afterOrientation ==  StageOrientation.UPSIDE_DOWN) {
				event.preventDefault();
			}
		}
		
		protected function _handlerLoaderInfoComplete(event:Event):void {
			_initialize();
		}
		
		
	}
}