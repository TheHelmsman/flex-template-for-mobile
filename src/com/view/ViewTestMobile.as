package com.view{
	import com.contex.ContextTestMobile;
	import com.view.mainPage.ViewMainPage;
	
	import feathers.themes.MetalWorksMobileTheme;
	
	import starling.display.Sprite;
	import starling.events.Event;
	
	public class ViewTestMobile extends Sprite{	
		//--------------------------------------------------------------------------------------------------------- 
		// 
		//  PUBLIC & INTERNAL VARIABLES 
		// 
		//---------------------------------------------------------------------------------------------------------
		
		
		//--------------------------------------------------------------------------------------------------------- 
		//
		// PRIVATE & PROTECTED VARIABLES
		//
		//---------------------------------------------------------------------------------------------------------
		private var _context:ContextTestMobile;
	
		private var _mainPage:ViewMainPage;
		
		//--------------------------------------------------------------------------------------------------------- 
		//
		//  CONSTRUCTOR 
		// 
		//---------------------------------------------------------------------------------------------------------
		public function ViewTestMobile(){
			//we'll initialize things after we've been added to the stage
			addEventListener(Event.ADDED_TO_STAGE, _handlerAddedToStage);
		}
		//--------------------------------------------------------------------------------------------------------- 
		// 
		//  GETTERS & SETTERS  
		// 
		//---------------------------------------------------------------------------------------------------------
		
		
		//--------------------------------------------------------------------------------------------------------- 
		// 
		//  PUBLIC & INTERNAL METHODS 
		// 
		//---------------------------------------------------------------------------------------------------------
		//--------------------------------------------------------------------------------------------------------- 
		//
		// PRIVATE & PROTECTED METHODS 
		//
		//--------------------------------------------------------------------------------------------------------
		private function _initialize():void{
			// init feathers theme
			new MetalWorksMobileTheme();
			
			_mainPageAdd();
			
			_context = new ContextTestMobile(this);
		}
		
		/**
		 * add page
		 * */
		private function _mainPageAdd():void{
			_mainPage = new ViewMainPage();
			addChild(_mainPage);
		}
		//--------------------------------------------------------------------------------------------------------- 
		// 
		//  EVENT HANDLERS  
		// 
		//---------------------------------------------------------------------------------------------------------
		protected function _handlerAddedToStage(event:Event):void {
			removeEventListener(Event.ADDED_TO_STAGE, _handlerAddedToStage);
			_initialize();
		}
		
	}
}