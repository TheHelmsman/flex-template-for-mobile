package com.view.mainPage{
	import feathers.controls.Button;
	
	import flash.geom.Point;
	
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	
	public class ViewMainPage extends Sprite{	
		//--------------------------------------------------------------------------------------------------------- 
		// 
		//  PUBLIC & INTERNAL VARIABLES 
		// 
		//---------------------------------------------------------------------------------------------------------
		
		
		//--------------------------------------------------------------------------------------------------------- 
		//
		// PRIVATE & PROTECTED VARIABLES
		//
		//---------------------------------------------------------------------------------------------------------
		
		private var _quad:Quad;
		private var _button:Button;
		private var _sprite:Sprite;
		
		private var _pointSpriteStartMotion:Point;
		//--------------------------------------------------------------------------------------------------------- 
		//
		//  CONSTRUCTOR 
		// 
		//---------------------------------------------------------------------------------------------------------
		public function ViewMainPage(){
			super();
			addEventListener(Event.ADDED_TO_STAGE, _handlerAddedToStage);
		}
		//--------------------------------------------------------------------------------------------------------- 
		// 
		//  GETTERS & SETTERS  
		// 
		//---------------------------------------------------------------------------------------------------------
		
		
		//--------------------------------------------------------------------------------------------------------- 
		// 
		//  PUBLIC & INTERNAL METHODS 
		// 
		//---------------------------------------------------------------------------------------------------------
		
		//--------------------------------------------------------------------------------------------------------- 
		//
		// PRIVATE & PROTECTED METHODS 
		//
		//--------------------------------------------------------------------------------------------------------
		
		protected function _initialize():void {
			_layoutCreate();
		}
		
		/**
		 * create and add content to the stage
		 * */
		private function _layoutCreate():void{
			trace("layout create")
			_sprite = new Sprite();
			addChild(_sprite);
			
			_quad = new Quad(100,100,0x00ff00);
			_sprite.addChild(_quad);
			
			_button = new Button();
			_button.label = "Test"
			_button.width = 150;
			_button.height = 100;
			_button.x = 250
			_button.validate()
			addChild(_button)
			
			_sprite.addEventListener(TouchEvent.TOUCH, _handlerButtonTouch);
		}
		//--------------------------------------------------------------------------------------------------------- 
		// 
		//  EVENT HANDLERS  
		// 
		//---------------------------------------------------------------------------------------------------------
		private function _handlerAddedToStage(event:Event):void{
			removeEventListener(Event.ADDED_TO_STAGE, _handlerAddedToStage);
			_initialize();
		}
		
		
		
		/**
		 * handler that responds to the touch event
		 * */
		private function _handlerButtonTouch(event:TouchEvent):void{
			var touches:Vector.<Touch> = event.getTouches(_sprite);
			var pTouchStart:Touch;
			
			for each (var touch:Touch in touches){
				var pLocation:Point = touch.getLocation(this);
				
				if(touch.phase  == TouchPhase.BEGAN){
					pTouchStart = event.getTouch(_sprite, TouchPhase.BEGAN);
					
					if(pTouchStart){
						var pPointGlobal:Point = pTouchStart.getLocation(this);
						var pPointLocal:Point = _sprite.globalToLocal(pPointGlobal);
						_pointSpriteStartMotion = pPointLocal;
					}
					
				}else if (touch.phase == TouchPhase.MOVED){
					
					pTouchStart = event.getTouch(this); 
					_sprite.x = pLocation.x - _pointSpriteStartMotion.x;
					_sprite.y = pLocation.y - _pointSpriteStartMotion.y;
					
				}
			}
		}
	}
	
	
}