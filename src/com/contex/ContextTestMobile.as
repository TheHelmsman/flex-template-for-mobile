package com.contex{
	import com.commands.CommandStartup;
	import com.view.MediatorViewTestMobile;
	import com.view.ViewTestMobile;
	import com.view.mainPage.MediatorViewMainPage;
	import com.view.mainPage.ViewMainPage;
	
	import org.robotlegs.base.ContextEvent;
	import org.robotlegs.mvcs.StarlingContext;
	
	import starling.display.DisplayObjectContainer;

	public class ContextTestMobile extends StarlingContext{	
		//--------------------------------------------------------------------------------------------------------- 
		// 
		//  PUBLIC & INTERNAL VARIABLES 
		// 
		//---------------------------------------------------------------------------------------------------------
		
		
		//--------------------------------------------------------------------------------------------------------- 
		//
		// PRIVATE & PROTECTED VARIABLES
		//
		//---------------------------------------------------------------------------------------------------------
		
		
		//--------------------------------------------------------------------------------------------------------- 
		//
		//  CONSTRUCTOR 
		// 
		//---------------------------------------------------------------------------------------------------------
		public function ContextTestMobile(contextView:DisplayObjectContainer=null, autoStartup:Boolean=true) {
			super(contextView, autoStartup);
		}
		//--------------------------------------------------------------------------------------------------------- 
		// 
		//  GETTERS & SETTERS  
		// 
		//---------------------------------------------------------------------------------------------------------
		
		
		//--------------------------------------------------------------------------------------------------------- 
		// 
		//  PUBLIC & INTERNAL METHODS 
		// 
		//---------------------------------------------------------------------------------------------------------
		override public function startup():void {
			// Startup - oneshot command
			commandMap.mapEvent(ContextEvent.STARTUP_COMPLETE, CommandStartup, ContextEvent, true);
			
		
			mediatorMap.mapView(ViewMainPage, MediatorViewMainPage);
			mediatorMap.mapView(ViewTestMobile, MediatorViewTestMobile);
			
			
			// Startup complete
			super.startup();
		}
		
		//--------------------------------------------------------------------------------------------------------- 
		//
		// PRIVATE & PROTECTED METHODS 
		//
		//--------------------------------------------------------------------------------------------------------
		
		
		//--------------------------------------------------------------------------------------------------------- 
		// 
		//  EVENT HANDLERS  
		// 
		//---------------------------------------------------------------------------------------------------------
		
		
		
	}
}